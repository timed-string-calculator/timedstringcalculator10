﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class Calculator
    {
        private readonly string customDelmitersId = "//";
        private readonly string newline = "\n";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string numberSection = GetNumberSection(numbers);
            string[] delimiters = GetDelmiters(numbers);
            List<int> numbersList = GetNumbers(numberSection, delimiters);

            ValidateNumbers(numbersList);

            return GetSum(numbersList);
        }

        private void ValidateNumbers(List<int> numbersList)
        {
            List<string> negativeNumber = new List<string>();

            foreach (var number in numbersList)
            {
                if (number < 0)
                {
                    negativeNumber.Add(number.ToString());
                }
            }

            if (negativeNumber.Count != 0)
            {
                throw new Exception($"Negative not allowed {string.Join(", ", negativeNumber)}");
            }
        }

        private string[] GetDelmiters(string numbers)
        {
            var multipleCustomDelimitersId = $"{customDelmitersId}[";
            var multipleCustomDelimitersSeperator = $"]{newline}";
            var multipleCustomDelimitersSplitter = "][";

            if (numbers.StartsWith(customDelmitersId))
            {
                string delmiters = numbers.Substring(numbers.IndexOf(multipleCustomDelimitersId) + multipleCustomDelimitersId.Length, numbers.IndexOf(multipleCustomDelimitersSeperator) - (multipleCustomDelimitersSeperator.Length + 1));

                return delmiters.Split(new[] { multipleCustomDelimitersSplitter }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (numbers.StartsWith(customDelmitersId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customDelmitersId) + customDelmitersId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", newline };
        }

        private string GetNumberSection(string numbers)
        {
            if (numbers.StartsWith(customDelmitersId))
            {
                return numbers.Substring(numbers.IndexOf(newline) + 1);
            }

            return numbers;
        }

        private int GetSum(List<int> numbersList)
        {
            int sum = 0;

            foreach (var number in numbersList)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }

        private List<int> GetNumbers(string numbers, string[] delimiters)
        {
            List<int> numbersList = new List<int>();
            string[] numbersArray = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            foreach (var num in numbersArray)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersList.Add(number);
                }
            }

            return numbersList;

        }
    }
}
